﻿namespace SlackEntity.Models
{
    public class MailSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string AuthEmail { get; set; }
        public string AuthPassword { get; set; }
    }
}