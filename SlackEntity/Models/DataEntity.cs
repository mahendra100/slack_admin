﻿using System;

namespace EstimateTool.Core.Model
{
    public abstract class DataEntity
    {
        public int Id { get; set; }
    }
}