﻿using EstimateTool.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlackEntity.Models
{
    public class User : DataEntity
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public int AccountType { get; set; }
        public int BillingStatus { get; set; }
        public int Status { get; set; }
    }
}