﻿using Microsoft.EntityFrameworkCore;
using SlackEntity.Models;

namespace SlackEntity
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> User { get; set; }
    }
}