﻿using EstimateTool.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Slack.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Slack.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SlackPolicy")]
    [ApiController]
    public class GenericController<T> : ControllerBase where T : DataEntity
    {
        private readonly IGenericRepository<T> _genericRepository;

        public GenericController(IGenericRepository<T> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        [HttpPost("Add")]
        public ActionResult<T> AddEntity([FromBody] T entityToAdd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var result = _genericRepository.AddEntity(entityToAdd);
                //Save();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpDelete("Delete/{guid}")]
        public ActionResult DeleteEntity(int guid)
        {
            try
            {
                var resultById = _genericRepository.GetEntityById(guid);
                _genericRepository.DeleteEntity(resultById);
                return Ok(new JsonResult("Delete Success"));
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet("GetAll")]
        public ActionResult<IEnumerable<T>> GetAll()
        {
            try
            {
                var result = _genericRepository.GetAll();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("GetById/{guid}")]
        public ActionResult<T> GetEntityById(int guid)
        {
            try
            {
                var result = _genericRepository.GetEntityById(guid);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public T UpdateEntity(T entityToUpdate)
        {
            var result = _genericRepository.UpdateEntity(entityToUpdate);
            return result;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void Save()
        {
            _genericRepository.Save();
        }
    }
}