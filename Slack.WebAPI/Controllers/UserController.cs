﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Slack.Services.Interfaces;
using Slack.Services.Services;
using SlackEntity.Models;

namespace Slack.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("SlackPolicy")]
    public class UserController : GenericController<User>
    {
        private UserService _userService;

        public UserController(IGenericRepository<User> genericRepository, UserService userService) : base(genericRepository)
        {
            _userService = userService;
        }

        [HttpGet("GetOwner")]
        public ActionResult GetAllOwner()
        {
            var owner = _userService.GetOwner();
            return Ok(owner);
        }

        #region Account Type

        [HttpGet("GetAdmin")]
        public ActionResult GetAllAdmin()
        {
            var owner = _userService.GetAdmin();
            return Ok(owner);
        }

        [HttpGet("GetMember")]
        public ActionResult GetAllFullMember()
        {
            var owner = _userService.GetMember();
            return Ok(owner);
        }

        [HttpGet("GetGuest")]
        public ActionResult GetAllGuest()
        {
            var owner = _userService.GetGuest();
            return Ok(owner);
        }

        #endregion Account Type

        #region Billing Status

        [HttpGet("GetActive")]
        public ActionResult GetActive()
        {
            var owner = _userService.GetActive();
            return Ok(owner);
        }

        [HttpGet("GetInactive")]
        public ActionResult GetInactive()
        {
            var owner = _userService.GetInactive();
            return Ok(owner);
        }

        #endregion Billing Status

        [HttpGet("ChangeToAdmin/{id}")]
        public ActionResult ChangeToAdmin(int id)
        {
            var result = GetEntityById(id);
            var userData = ((ObjectResult)result.Result).Value as User;
            userData.Status = 2;
            userData.AccountType = 2;
            var updatedUser = UpdateEntity(userData);
            return Ok(updatedUser);
        }

        [HttpPost("SendInvitation")]
        public ActionResult SendInvitation([FromBody] string[] emailList)
        {
            _userService.SendInvitation(emailList);
            return Ok();
        }
    }
}