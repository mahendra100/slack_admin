﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Slack.Services.Interfaces;
using Slack.Services.Services;
using SlackEntity;
using SlackEntity.Models;

namespace Slack.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(option => option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IGenericRepository<User>, GenericRepository<User>>();
            services.AddTransient<UserService>();
            services.AddTransient<MailSender>();
            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
            services.AddOptions();

            services.AddCors(options =>
            {
                options.AddPolicy("SlackPolicy",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("SlackPolicy");
            app.UseCors(builder =>
                builder.WithOrigins("https://localhost:4200"));

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}