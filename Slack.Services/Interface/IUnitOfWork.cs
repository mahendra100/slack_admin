﻿using SlackEntity.Models;
using System;

namespace Slack.Services.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<User> UserRepository { get; }
    }
}