﻿using EstimateTool.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Slack.Services.Interfaces
{
    public interface IGenericRepository<T> where T : DataEntity
    {
        T AddEntity(T entityToAdd);

        T GetEntityById(int guid);

        IEnumerable<T> GetAll();

        void DeleteEntity(T entityToDelete);

        T UpdateEntity(T entityToUpdate);

        IEnumerable<T> FindByCondition(Expression<Func<T, bool>> predicate);

        void Save();
    }
}