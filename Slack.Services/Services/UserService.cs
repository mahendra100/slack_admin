﻿using Microsoft.Extensions.Options;
using Slack.Services.Interfaces;
using SlackEntity;
using SlackEntity.Models;
using System.Collections.Generic;
using System.Linq;

namespace Slack.Services.Services
{
    public class UserService
    {
        private IGenericRepository<User> _userRepo;
        private ApplicationDbContext _context;
        private readonly MailSender _mailSender;
        private readonly IOptions<MailSettings> _mailSettings;

        public UserService(IGenericRepository<User> userRepo, ApplicationDbContext context, MailSender mailSender, IOptions<MailSettings> mailsettings)
        {
            _userRepo = userRepo;
            _context = context;
            _mailSender = mailSender;
            _mailSettings = mailsettings;
        }

        #region AccountType

        public List<User> GetOwner()
        {
            List<User> owner = _context.User.Where(x => x.AccountType == 1).ToList();
            return owner;
        }

        public List<User> GetAdmin()
        {
            List<User> admin = _context.User.Where(x => x.AccountType == 2).ToList();
            return admin;
        }

        public List<User> GetMember()
        {
            List<User> member = _context.User.Where(x => x.AccountType == 3).ToList();
            return member;
        }

        public List<User> GetGuest()
        {
            List<User> guest = _context.User.Where(x => x.AccountType == 4).ToList();
            return guest;
        }

        #endregion AccountType

        #region Billing Status

        public List<User> GetActive()
        {
            List<User> activeAccount = _context.User.Where(x => x.BillingStatus == 1).ToList();
            return activeAccount;
        }

        public List<User> GetInactive()
        {
            List<User> inactiveAccount = _context.User.Where(x => x.BillingStatus == 2).ToList();
            return inactiveAccount;
        }

        #endregion Billing Status

        public void SendInvitation(string[] email)
        {
            List<string> emailList = new List<string>();
            foreach (var userEmail in email)
            {
                emailList.Add(userEmail);
            }
            string subject = "Slack Invitation";
            string body = $"ADMIN: <br> You are invited to join the slack team insightworkshop : Please open link below <br> https://mail.google.com/mail/u/0/h/1p508u5kur0rs/?&th=1644655909941152&v=c&st=200";
            _mailSender.SendMail(emailList, subject, body, _mailSettings);
        }
    }
}