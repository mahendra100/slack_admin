﻿using Slack.Services.Interfaces;
using SlackEntity;
using SlackEntity.Models;
using System;

namespace Slack.Services.Services
{
    internal class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _dbContext;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private IGenericRepository<User> _userRepostitory { get; set; }

        public IGenericRepository<User> UserRepository => _userRepostitory ?? (_userRepostitory = new GenericRepository<User>(_dbContext));

        IGenericRepository<User> IUnitOfWork.UserRepository => throw new NotImplementedException();

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}