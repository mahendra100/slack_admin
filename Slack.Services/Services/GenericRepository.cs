﻿using EstimateTool.Core.Model;
using Microsoft.EntityFrameworkCore;
using Slack.Services.Interfaces;
using SlackEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Slack.Services.Services
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : DataEntity
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException("Context was not supplied.");
            _dbSet = _context.Set<TEntity>();
        }

        public TEntity AddEntity(TEntity entityToAdd)
        {
            _dbSet.Add(entityToAdd);
            Save();
            return entityToAdd;
        }

        public void DeleteEntity(TEntity entityToDelete)
        {
            _dbSet.Attach(entityToDelete);
            _dbSet.Remove(entityToDelete);
            Save();
        }

        public TEntity UpdateEntity(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
            Save();
            return entityToUpdate;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.AsEnumerable();
        }

        public IEnumerable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public TEntity GetEntityById(int guid) => _dbSet.Find(guid);

        public void Save() => _context.SaveChanges();

        public async Task<int> SaveAsync() => await _context.SaveChangesAsync();
    }
}